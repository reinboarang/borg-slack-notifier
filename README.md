## Usage

`$ ./borg-slack-notifier <compiled log file>`

This script assumes the existence of a log file that contains the output of
a borg create command with both the `--json` and `--log-json` switches and
their respective outputs redirected to the same file.

## Example create command:

`$ borg create --json --log-json [repo]::[archive] [folder_or_file] >> [log_file] 2>&1`

For the script to be able to post messages to Slack, you must create a webhook from Slack's API website and copy its URL into settings.json.
borg-slack-notifier.rb can be invoked from a cron job in order to create a report of the results of all backups made within a certain period
of time.
