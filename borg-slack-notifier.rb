#!/usr/bin/ruby

require './common.rb'
require 'date'
require 'pry'

def parse_daily_log filename, log_items = {}
	case filename
	when String # used on the initial call
		logfile = File.new(filename, 'r')
	when File # used for recursive calls in order to reuse the File object
		logfile = filename
	end

	# parse and append log-json messages
	cur_archive_messages = []
	while (cur_line = logfile.readline) != "{\n" do # a single left brace on a line denotes the end of log messages
		cur_archive_messages << JSON.parse(cur_line) unless cur_line.empty?
	end

	# construct and parse the command result json
	borg_create_result_str = cur_line
	begin
		cur_line = logfile.readline
		borg_create_result_str += cur_line
	end while cur_line != "}\n"
	
	borg_create_result = JSON.parse(borg_create_result_str) # once all of the command result json lines have been appended, parse it

	# add results to the hash in the form `log_items[archive_name][log]` for log-json messages
	# and `log_items[archive_name][result]` for the command result json
	log_items[borg_create_result["archive"]["name"]] = {}
	log_items[borg_create_result["archive"]["name"]]["log"] = cur_archive_messages
	log_items[borg_create_result["archive"]["name"]]["result"] = borg_create_result

	if logfile.eof?
		return log_items
	else
		parse_daily_log logfile, log_items
	end
end

# filter out all log messages which aren't errors of some level
def select_log_errors log_items
	error_items = {}
	log_items.each_pair do |k,v|
		error_items[k] = v["log"].select { |x| x["levelname"] != nil }
	end
	error_items
end

ERROR_LEVEL = { 
	"INFO" => 0,
	"WARNING" => 1,
	"ERROR" => 2,
	"CRITICAL" => 3
}

# Looks for the highest error level message in a single archive's log and returns it as a string 
def get_error_level error_archive
	error_archive.reduce("INFO") do |a,x|
		return x["levelname"] if ERROR_LEVEL[a] < ERROR_LEVEL[x["levelname"]]
		return a
	end
end

# Slims down a full archive to a hash in the form of { <archive_name> => <error_level>, ... }
def classify_archives error_log
	error_log.map { |k,v| [k, get_error_level(v)] }.to_h
end

# Formats a list of strings to be posted directly to Slack
def create_slack_list name, archives
	message = ">*#{name}:*\n"
	message += "> _None_\n>\n" if archives.empty?
	message += archives.map do |x|
		">- `" + x + "`"
	end.join("\n") + "\n>\n" if !archives.empty?
	message
end

# extract the name of the repo from an archive's log
def get_log_repo name, full_log
	location = full_log[name]["result"]["repository"]["location"]
	location[location.rindex('/')+1..-1]
end

# get the associated webhook url from a settings file for the specified repo name
def get_repo_webhook repo, settings = read_settings("./settings.json")
  settings["webhooks"][repo]
end

def create_report report_hash
  slack_message = "*#{DateTime.now.rfc822}*\n"
	slack_message += create_slack_list("Completed successfully", report_hash[:info])
	slack_message += create_slack_list("Completed with errors", report_hash[:warn])
	slack_message += create_slack_list("Failed", report_hash[:fail])
  slack_message
end

# The only function you need to use
def post_report log_file, settings = read_settings("./settings.json")
	full_log = parse_daily_log(log_file)
	error_log = classify_archives(select_log_errors(full_log))
  repo_reports = {}
  # TODO: couple borg repos to webhook urls and post reports for those repos only to their respective webhooks
  error_log.each do |k,v|
    repo_name = get_log_repo(k, full_log)
    repo_reports[repo_name] = {info: [], warn: [], fail: []} unless repo_reports[repo_name]
    repo_reports["all"] = {info: [], warn: [], fail: []} unless repo_reports["all"]
    case v
    when "INFO"
      repo_reports[repo_name][:info] << k
      repo_reports["all"][:info] << k
    when "WARNING"
      repo_reports[repo_name][:warn] << k
      repo_reports["all"][:warn] << k
    when "ERROR", "CRITICAL"
      repo_reports[repo_name][:fail] << k
      repo_reports["all"][:fail] << k
    end
  end
  repo_reports.each do |k,v|
    webhook = get_repo_webhook(k, settings)
    post_to_slack("*Configuration Error:* Webhook URL for `#{k}` repo is not specified in settings.", get_repo_webhook("error", settings)) unless webhook
    post_to_slack(create_report(v), webhook) if webhook
  end
end

if ARGV.size < 1
	puts("*Configuration Error:* Log filename not given.")
	post_to_slack("*Configuration Error:* Log filename not given.", get_repo_webhook("error"))
end
post_report(ARGV[0])

# vim: tabstop=2 shiftwidth=2 expandtab
