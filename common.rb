require 'faraday'
require 'json'

def read_settings file
	JSON.parse(File.open(file).read)
end

def post_to_slack msg, webhook
	webhook_url = webhook
	case webhook_url
	when String
		Faraday.post(webhook_url.strip, "{\"text\" : \"#{msg}\"}")
	when Array
    webhook_url.each { |wh| post_to_slack(msg, wh) }
	else
    raise "Invalid value type for webhook-url in configuration: #{webhook_url.class}"
	end
end

# vim: tabstop=2 shiftwidth=2 expandtab
